
import android.os.Bundle
import androidx.activity.compose.setContent
import androidx.appcompat.app.AppCompatActivity
import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.*
import androidx.compose.material3.Button
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import com.example.a3pantallas.R
import kotlin.random.Random

class MainActivity : AppCompatActivity() {

    @Composable
    fun HuertoBotones() {
        var estado by remember { mutableStateOf(Estado.HUERTO_VACIO) }
        var contadorFrutas by remember { mutableStateOf(0) }

        Column(
            modifier = Modifier
                .fillMaxSize()
                .padding(16.dp),
            verticalArrangement = Arrangement.Center,
            horizontalAlignment = Alignment.CenterHorizontally
        ) {
            HuertoImagen(estado)
            Spacer(modifier = Modifier.height(16.dp))
            Button(
                onClick = {
                    when (estado) {
                        Estado.HUERTO_VACIO -> {
                            estado = Estado.PLANTAS
                        }

                        Estado.PLANTAS -> {
                            estado = Estado.FRUTAS
                        }

                        Estado.FRUTAS -> {
                            contadorFrutas++
                            val valorEuros = Random.nextInt(1, 6)
                            estado = Estado.HUERTO_VACIO
                        }
                    }
                }
            ) {
                Text(text = "Pulsar")
            }
        }
    }

    @Composable
    fun HuertoImagen(estado: Estado) {
        val imageResource = when (estado) {
            Estado.HUERTO_VACIO -> R.drawable.huerto_vacio
            Estado.PLANTAS -> R.drawable.huerto_planta
            Estado.FRUTAS -> R.drawable.huerto_frutas
        }

        val context = LocalContext.current

        Image(
            painter = painterResource(id = imageResource),
            contentDescription = null
        )
    }

    @Composable
    fun showRandomEuros(valorEuros: Int) {
        val context = LocalContext.current
        Text(
            text = "Ganaste $valorEuros euros",
            style = MaterialTheme.typography.bodyMedium
        )
    }

    enum class Estado {
        HUERTO_VACIO,
        PLANTAS,
        FRUTAS
    }
}


